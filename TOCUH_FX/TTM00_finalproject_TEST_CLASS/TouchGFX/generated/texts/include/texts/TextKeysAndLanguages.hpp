/* DO NOT EDIT THIS FILE */
/* This file is autogenerated by the text-database code generator */

#ifndef TOUCHGFX_TEXTKEYSANDLANGUAGES_HPP
#define TOUCHGFX_TEXTKEYSANDLANGUAGES_HPP

enum LANGUAGES
{
    GB,
    NUMBER_OF_LANGUAGES
};

enum TEXTS
{
    T_RESOURCEID1,
    T___SINGLEUSE_T5Q3,
    T___SINGLEUSE_IJ8U,
    T___SINGLEUSE_MC74,
    T___SINGLEUSE_CEB7,
    NUMBER_OF_TEXT_KEYS
};

#endif // TOUCHGFX_TEXTKEYSANDLANGUAGES_HPP
