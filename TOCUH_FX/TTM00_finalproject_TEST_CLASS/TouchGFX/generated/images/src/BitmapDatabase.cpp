// 4.19.1 0x8a602c5e
// Generated by imageconverter. Please, do not edit!

#include <BitmapDatabase.hpp>
#include <touchgfx/Bitmap.hpp>

extern const unsigned char image_blue_buttons_round_small[]; // BITMAP_BLUE_BUTTONS_ROUND_SMALL_ID = 0, Size: 170x60 pixels
extern const unsigned char image_blue_buttons_round_small_pressed[]; // BITMAP_BLUE_BUTTONS_ROUND_SMALL_PRESSED_ID = 1, Size: 170x60 pixels
extern const unsigned char image_boton_rojo_si[]; // BITMAP_BOTON_ROJO_SI_ID = 2, Size: 512x512 pixels
extern const unsigned char image_button_png_5a373e367a4d20_841205321513569846501_removebg_preview[]; // BITMAP_BUTTON_PNG_5A373E367A4D20_841205321513569846501_REMOVEBG_PREVIEW_ID = 3, Size: 657x380 pixels
extern const unsigned char image_cielo_azul_con_nubes_blancas_fondo_vertical_espacio_error_su_propio_texto_204[]; // BITMAP_CIELO_AZUL_CON_NUBES_BLANCAS_FONDO_VERTICAL_ESPACIO_ERROR_SU_PROPIO_TEXTO_204_ID = 4, Size: 600x900 pixels

const touchgfx::Bitmap::BitmapData bitmap_database[] = {
    { image_blue_buttons_round_small, 0, 170, 60, 23, 5, 124, ((uint8_t)touchgfx::Bitmap::ARGB8888) >> 3, 48, ((uint8_t)touchgfx::Bitmap::ARGB8888) & 0x7 },
    { image_blue_buttons_round_small_pressed, 0, 170, 60, 23, 5, 124, ((uint8_t)touchgfx::Bitmap::ARGB8888) >> 3, 48, ((uint8_t)touchgfx::Bitmap::ARGB8888) & 0x7 },
    { image_boton_rojo_si, 0, 512, 512, 139, 150, 231, ((uint8_t)touchgfx::Bitmap::ARGB8888) >> 3, 222, ((uint8_t)touchgfx::Bitmap::ARGB8888) & 0x7 },
    { image_button_png_5a373e367a4d20_841205321513569846501_removebg_preview, 0, 657, 380, 197, 58, 264, ((uint8_t)touchgfx::Bitmap::ARGB8888) >> 3, 263, ((uint8_t)touchgfx::Bitmap::ARGB8888) & 0x7 },
    { image_cielo_azul_con_nubes_blancas_fondo_vertical_espacio_error_su_propio_texto_204, 0, 600, 900, 0, 0, 600, ((uint8_t)touchgfx::Bitmap::RGB565) >> 3, 900, ((uint8_t)touchgfx::Bitmap::RGB565) & 0x7 }
};

namespace BitmapDatabase
{
const touchgfx::Bitmap::BitmapData* getInstance()
{
    return bitmap_database;
}

uint16_t getInstanceSize()
{
    return (uint16_t)(sizeof(bitmap_database) / sizeof(touchgfx::Bitmap::BitmapData));
}
} // namespace BitmapDatabase
